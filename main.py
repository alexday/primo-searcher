"""
Purpose: To scrape all of the PRIMO Catalog IDs from the titles in an Excel document
Author: Alex Day
Date: October 8th 2018
"""

"""
Things That Confuse:
1) Periods
2) Missing "The"
3) AJMR
4) Differing Punctuation (American Education / United States Department of Education vs American education (United States. Department of Education))
5) When two versions of a record exist
"""

# Import libraries
from bs4 import BeautifulSoup
# noinspection PyProtectedMember
from bs4 import Tag
from selenium import webdriver
from pandas import read_excel
from re import search
from time import sleep

# Define constants for string formatting
SEARCH_URL_TEMPLATE = "https://sshelco-primo.hosted.exlibrisgroup.com/primo-explore/search?query=any,contains," \
                      "{}&tab=alma_tab&search_scope=alma_scope&vid=CLARION&offset=0"
NEW_URL_TEMPLATE = "https://sshelco-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid={" \
                   "}&context=L&vid=CLARION&search_scope=alma_scope&isFrbr=true&tab=alma_tab&lang=en_US"

# Create a new firefox window for the web scraping
driver = webdriver.Firefox()
# Tell the web driver to wait ten seconds for the page to load
driver.implicitly_wait(20)

def normalize_search_term(search_term: str) -> str:
    return search_term.lower().replace(".", "").replace("the", "").strip()


def query_database(search_term: str, i:int=0) -> str or bool:
    """
    Query the PRIMO database to get the link URL for the search term
    :param search_term: Term to search the database for
    :return: Link to the search results
    """

    # Format the URL with the search term, replacing all spaces with "%20"
    search_url = SEARCH_URL_TEMPLATE.format(search_term.replace(" ", "%20"))


    # Get the page with the driver
    driver.get(search_url)
    # Blocking call until an element that matches the xpath is found. Throws
    # an error if it waits more than ten seconds
    driver.find_element_by_xpath("//div[starts-with(@id, 'SEARCH_RESULT')]")

    # Create a BS4 soup with the lxml parser from the current page source
    soup = BeautifulSoup(driver.page_source, "lxml")

    # Iterate through every DOM object on the page that matches the search result check
    for result in soup.find_all(is_search_result):
        # Iterate through every link that has a certain class in the result div
        for link in result.find_all("a", {"class": "md-primoExplore-theme"}):
            # Return the HREF from the first link
            for span in result.find_all("span", {"ng-bind-html": "$ctrl.highlightedText"}):
                if normalize_search_term(span.text) == normalize_search_term(search_term.lower()):
                    return link.get("href")

    return False
                
def extract_primo_record(details_url: str) -> str:
    """
    Extract the PRIMO Catalog ID from a details view url
    :param details_url: URL from the search result
    :return: PRIMO Catalog ID
    """
    # Return a REGEX match with the fluff replaced off of the end
    return str(search('docid=(.*)&context', details_url).group(1)).replace("docid=", "").replace("&cont", "")


def is_search_result(tag: Tag) -> bool:
    """
    Given a BeautifulSoup tag, determine if that tag is a search result
    :param tag: Tag from the soup
    :return: True if search result, else false
    """
    return str(tag.get("id")).count("SEARCH_RESULT") > 0


def test_first_n(n_to_test: int) -> None:
    """
    Test the first n lines from the excel document, assuming they are filled
    :param n_to_test: Number of lines to test
    :return: True if all passed, False if one failed
    """
    # Read the excel file
    excel = read_excel("Carlson Periodicals File.xlsx")

    # Iterate through n zipped title and urls
    for record in zip(excel[:n_to_test]["Title"], excel[:n_to_test]["New URL"]):
        # Get the PRIMO record from the database
        try:
            primo_record = query_database(record[0])
            if not primo_record:
                print("Could not find result for: ", record[0])
                continue

            # Construct a new URL from the primo record
            new_url = NEW_URL_TEMPLATE.format(extract_primo_record(primo_record))

            # If the two do not match
            if not new_url.replace("\n", "") == record[1].replace("\n", ""):
                # Print the non-matching URLS
                print("Failed on: ", record[0])
                print(new_url.replace("\n", ""))
                print(record[1].replace("\n", ""))
        except Exception:
            print("Browser took too long to load on ", record[0], " skipping...")

def do_first_n(n_to_start: int, n_to_do: int) -> None:
    # Read the excel file
    excel = read_excel("Carlson Microfilm File.xlsx")

    # Iterate through n zipped title and urls
    for i in range(n_to_start, n_to_do):
        # Get the PRIMO record from the database
        try:
            primo_record = query_database(excel["PrintISSN"][i])
            
            if not primo_record:
                primo_record = query_database(excel["Title"][i])

            # Construct a new URL from the primo record
            new_url = NEW_URL_TEMPLATE.format(extract_primo_record(primo_record))

            excel.loc[(i, "New URL")] = new_url

            if not primo_record:
                excel.loc[(i, "New URL")] = "Not Found"
        
            
        except Exception as e:
            print(str(e))
            excel.loc[(i, "New URL")] = "TIMEOUT"
        if i % 5 == 0:
            print("{}/{}".format(str(i), str(len(excel))))
            excel.to_excel("Temp/Not_Finished_{}.xlsx".format(str(i)))
        
    excel.to_excel("Finished.xlsx")


if __name__ == '__main__':
    do_first_n(70, 600)
